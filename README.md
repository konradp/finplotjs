# finplotjs

# Demo
https://konradp.gitlab.io/finplotjs

# Run locally
Run.
  npm install
  npm start

View on  
http://localhost:8000/

# Info
Following tutorial

https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes
