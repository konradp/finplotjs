import json
import random
with open('example1_data.json', 'r') as f:
  prices = json.load(f)
points = [ { 'date': i['date'], 'value': random.randint(-5,5) } for i in prices ]
data = {
  'points': points,
  'prices': prices,
}
with open('example6_data.json', 'w') as f:
  f.write(json.dumps(data, indent=2))
