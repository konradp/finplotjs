return;
var navbar_data = {
  "example1": "Candlestick from class",
  "example2": "Plot class: array data",
  "example3": "Multiple plots",
  "example4": "Button to change multiple plots",
  "example5": "Button to change multiple plots (+dates)",
  "example6": "JSON data",
  "example7": "Scale Y axis: max value",
  "example8": "Scale Y axis: min value",
  "example9": "Scale candlestick width and spacing",
  "example10": "Vertical lines",
}

var navbar = document.getElementById("navbar");
var list = document.createElement("ul");
for (var name in navbar_data) {
  var li = document.createElement("li");
  var a = document.createElement("a");
  a.href = name + ".html";
  a.innerHTML = name + ": " + navbar_data[name];
  li.appendChild(a);
  list.appendChild(li);
}
navbar.appendChild(list);
