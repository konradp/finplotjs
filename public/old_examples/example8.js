var canvas = document.getElementById('canvas');
var data = [
  {
    "date": "2019-07-01",
    "open": 100,
    "high": 150,
    "low": 100,
    "close": 110,
  },
  {
    "date": "2019-07-01",
    "open": 250,
    "high": 250,
    "low": 200,
    "close": 240,
  },
  {
    "date": "2019-07-01",
    "open": 150,
    "high": 240,
    "low": 110,
    "close": 170,
  },
  {
    "date": "2019-07-01",
    "open": 150,
    "high": 250,
    "low": 100,
    "close": 160,
  },
];

if (canvas.getContext) {
  var p = new Plot(canvas.getContext('2d'));
  p.setData(data);
  p.draw();
}
