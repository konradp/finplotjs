var data = [
  [ 20, 100, 10, 50 ],
  [ 50, 60, 10, 20 ],
  [ 50, 80, 40, 70 ],
  [ 40, 90, 30, 80 ],
];
let plot = new Plot('example_todo1_arrays', {dataOhlc: data});
plot.draw();
