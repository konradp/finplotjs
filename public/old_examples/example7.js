var canvas = document.getElementById('canvas');
var data = [
  {
    "date": "2019-07-01",
    "open": 120,
    "high": 200,
    "low": 90,
    "close": 150,
  },
  {
    "date": "2019-07-01",
    "open": 50,
    "high": 60,
    "low": 10,
    "close": 20,
  },
  {
    "date": "2019-07-01",
    "open": 50,
    "high": 80,
    "low": 40,
    "close": 70,
  },
  {
    "date": "2019-07-01",
    "open": 40,
    "high": 90,
    "low": 30,
    "close": 80,
  },
];

if (canvas.getContext) {
  var p = new Plot(canvas.getContext('2d'));
  p.setData(data);
  p.draw();
}
