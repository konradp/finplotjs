var kPlotNames = [ 'plot1', 'plot2', 'plot3' ];

function drawPlots(timeRange) {
  for (var i in kPlotNames) {
    var ticker = kPlotNames[i];
    var canvas = document.getElementById(ticker);
    if (canvas.getContext) {
      var p = new Plot(canvas.getContext('2d'));
      p.setData(
        getData(ticker, timeRange),
        [ 'date', 'open', 'high', 'low', 'close' ],
      );
      p.draw();
    }
  }
}

// MAIN
var plots = document.getElementById('plots');
for (var i in kPlotNames) {
  var c = document.createElement('canvas');
  c.id = kPlotNames[i];
  c.style = 'width:50%; height:20%; display: block;';
  plots.appendChild(c);
  plots.appendChild(document.createElement('br'));
}
drawPlots('1d');
