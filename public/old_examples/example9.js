var plots = document.getElementById('plots');
var keys = Array(12).keys();

for (i of keys) {
  // Generate data
  var data = [];
  for (var j = 0; j < Math.pow(i, 2) + 1; j++) {
    var k = Math.sin(j);
    data[j] = [
      k-Math.pow(-1,j),
      k+Math.pow(-1,j),
      k-Math.pow(-1,j),
      k + Math.pow(-1,j)
    ];
  }

  var c = document.createElement('canvas');
  c.id = i;
  c.width = 800;
  c.height = 800;
  c.style = 'width: 50%; height: 20%; display: block;';
  plots.appendChild(c);
  plots.appendChild(document.createElement('br'));
  var canvas = document.getElementById(i);
  if (canvas.getContext) {
    var p = new Plot(canvas.getContext('2d'));
    p.setData(data);
    p.draw();
  }
}
