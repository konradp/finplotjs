var data = [
  {
    "name": "plot1",
    "data": [
      [ 20, 100, 10, 50 ],
      [ 50, 60, 10, 20 ],
      [ 50, 80, 40, 70 ],
      [ 40, 90, 30, 80 ],
    ],
    "lines": [ null, "label1", null, "label2" ],
  },
  {
    "name": "plot2",
    "data": [
      { "date": "20190701", "open": 50, "high": 60,  "low": 10, "close": 20 },
      { "date": "20190702", "open": 40, "high": 90,  "low": 30, "close": 80 },
      { "date": "20190703", "open": 50, "high": 80,  "low": 40, "close": 70 },
      { "date": "20190704", "open": 20, "high": 100, "low": 10, "close": 50 },
    ],
    "lines": [
      { "date": "20190702", "label": "label1", },
      { "date": "20190704", "label": "label2", },
    ],
  },
  {
    "name": "plot3",
    "data": [
      [ 50, 80, 40, 70 ],
      [ 40, 90, 30, 80 ],
      [ 20, 100, 10, 50 ],
      [ 50, 60, 10, 20 ],
    ],
  },
];

// MAIN
var plots = document.getElementById('plots');
for (var i in data) {
  var c = document.createElement('canvas');
  c.id = data[i].name;
  c.style = 'width:50%; height:20%; display: block;';
  plots.appendChild(c);
  plots.appendChild(document.createElement('br'))

  // Draw plot
  var canvas = document.getElementById(data[i].name);
  if (canvas.getContext) {
    var p = new Plot(canvas.getContext('2d'));
    p.setData(data[i]['data']);
    if (data[i]['lines']) {
      p.setLines(data[i]['lines']);
    }
    p.draw();
  }
}
