fetch('data/example7_data.json')
  .then(response => response.json())
  .then(data => {
    plot = new Plot('example7');
    plot.setSettings({ hoverLabels: true });
    plot.setDataOhlc(data['prices']);
    plot.draw();
  });
