// Table with examples
window.addEventListener('load', Run);

function _create(tag) {
  return document.createElement(tag);
}

function Run() {
  let examples = document.getElementsByTagName('example');
  for (let ex of examples) {
    // Display example code
    let name = ex.getAttribute('src');
    let url = 'js/'+ name + '.js';
    fetch(url)
      .then((response => response.text()))
      .then(data => {
        let domPre = _create('pre');
        let domCode = _create('code');
        domCode.innerHTML = data;
        domCode.className = 'language-js';
        hljs.highlightElement(domCode); // highlightjs
        domPre.appendChild(domCode);
        ex.appendChild(domPre);
        // Run the code: unsafe eval
        eval(data);
      })
  }
}
