fetch('data/example8_data.json')
  .then(response => response.json())
  .then(data => {
    console.log('data', data);
    plot = new Plot('example8');
    plot.setSettings({
      showVolume: true,
      hoverLabels: true,
    });
    plot.setDataOhlc(data);
    plot.draw();
  });
