fetch('data/example6_data.json')
  .then(response => response.json())
  .then(data => {
    plot = new Plot('example6');
    plot.setSettings({ hoverLabels: true });
    plot.setDataOhlc(data['prices']);
    plot.setDataPoints( [ { "data": data['points'] } ]);
    plot.draw();
  });
