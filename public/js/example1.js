fetch('data/example1_data.json')
  .then(response => response.json())
  .then(data => {
    let plot = new Plot('example1', {
      dataOhlc: data,
    });
    plot.draw();
  });
