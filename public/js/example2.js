fetch('data/example1_data.json')
  .then(response => response.json())
  .then(data => {
    let dataOther = [
      {
        "label": "some label1",
        "color": "blue",
        "data": [
          { "date": "2021-01-27", "value": 3600, },
          { "date": "2021-02-05", "value": 3500, },
          { "date": "2021-02-18", "value": 3700, },
          { "date": "2021-03-01", "value": 3500, },
          { "date": "2021-03-08", "value": 3700, },
          { "date": "2021-03-12", "value": 3600, },
        ]
      }
    ];
    let plot = new Plot('example2', {
      dataOhlc: data,
      data: dataOther,
    });
    plot.draw();
  });
