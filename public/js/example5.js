fetch('data/example1_data.json')
  .then(response => response.json())
  .then(data => {
    plot = new Plot('example5');
    plot.setDataOhlc(data);
    plot.draw();
    plot.setOnMouseMove(function(e) {
      e.preventDefault();
      let canvas = this.canvas;
      var rect = canvas.getBoundingClientRect();
      let mousePos = this._mouseToPos(e.clientX, e.clientY);
      this.drawCrosshair(mousePos.x, mousePos.y);
      // custom: Draw a circle
      let c = this.c; // canvas context
      const radius = 10;
      c.beginPath();
      c.arc(mousePos.x, mousePos.y, radius, 0, Math.PI*2);
      c.stroke();
    });
  });
