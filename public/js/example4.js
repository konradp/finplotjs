fetch('data/example1_data.json')
  .then(response => response.json())
  .then(data => {
    plot = new Plot('example4', {
      settings: {
        showCrosshair: true,
      }
    });
    plot.setDataOhlc(data);
    plot.draw();
  });
