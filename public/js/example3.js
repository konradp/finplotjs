fetch('data/example1_data.json')
  .then(response => response.json())
  .then(data => {
    let plot = new Plot('example3', {dataOhlc: data});
    plot.setVlines([
      { "date": "2020-09-15", "label": "label1", },
      { "date": "2020-11-05", "label": "label2", },
      { "date": "2021-01-05", "label": "label2", },
    ]);
    plot.setHlines([ 3400, 3600 ]);
    plot.draw();
  });
