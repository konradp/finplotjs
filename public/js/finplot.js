class Plot {
  c = null; // canvas element
  dataOhlc = [];
  dataOther = [];
  dataPoints = [];
  settings = {
    showCrosshair: false,
    hoverLabels: false,
    showVolume: false,
  };
  vLines = [];
  hLines = [];
  spacing = 2;
  fLabels = {
    open: "open",
    high: "high",
    low: "low",
    close: "close"
  };
  candleWidth = null;
  // height of secondary graph (e.g. volume)
  volH = null;
  onMouseMoveUser = null;
  _mouseDragging = false;
  _mousePosStart = null;
  _mousePosEnd = null;


  constructor(canvas_id, data=null) {
    this.canvas = document.getElementById(canvas_id);
    this.canvas.width *= 2;
    this.canvas.height *= 2;
    this.c = this.canvas.getContext('2d');
    if (data != null) {
      if ('dataOhlc' in data) {
        this.setDataOhlc(data.dataOhlc);
      }
      if ('data' in data) {
        this.setData(data.data);
      }
      if ('settings' in data) {
        this.setSettings(data.settings);
      }
    }
    // Mouse events
    this.canvas.addEventListener('mousemove', this.onMouseMove.bind(this));
    this.canvas.addEventListener('mousedown', this.onMouseDown.bind(this));
    this.canvas.addEventListener('mouseup', this.onMouseUp.bind(this));
    this.canvas.addEventListener('mouseout', this.onMouseOut.bind(this));
  }


  /* SETTERS */
  setSettings(settings) {
    this.settings = { ...this.settings, ...settings };
  }


  setLabels(labels) {
    // Use this if the JSON data has got fields named other than
    // open,high,low,close, e.g. o,h,l,c
    // INPUT: Array, e.g.
    // [ 'o', 'h', 'l', 'c' ]
    this.fLabels = {
      open: labels[0],
      high: labels[1],
      low: labels[2],
      close: labels[3]
    };
  }


  setData(data) {
    this.dataOther = data;
  }


  setDataOhlc(data, fields = [ 'open', 'high', 'low', 'close' ]) {
    var f = fields;
    // Is data an array of arrays, or key-value pairs
    //this.dataType = (Array.isArray(data[0]))? 'array' : 'json';
    // Check required fields present
    [ 'open', 'high', 'low', 'close' ].forEach((i) => {
      if (fields.indexOf(i) == -1) {
        throw 'Required field missing: ' + i;
      }
    })
    this.dataOhlc = data;
    // Calculate min and max and other global params
    var l = this.fLabels;
    var hi = data[0][l.high];
    var lo = data[0][l.low];
    for (var i in data) {
      if (data[i][l.high] > hi)
        hi = data[i][l.high];
      if (data[i][l.low] < lo)
        lo = data[i][l.low];
    }
    var width = this.canvas.width;
    this.spacing = width/data.length;
    this.fields = fields;
    this.max = hi;
    this.min = lo;
    this.candleWidth = (0.8)*this.canvas.width/data.length; // candle width
    this.volH = this.candleWidth*5;
  };


  setDataPoints(data) {
    this.dataPoints = data;
  }


  setVlines(lines) {
    this.vLines = lines;
  }


  setHlines(lines) {
    this.hLines = lines;
  }


  setOnMouseMove(userFn) {
    if (typeof userFn === 'function') {
      this.onMouseMoveUser = userFn;
    } else {
      console.warn("Provided value is not a valid function.");
    }
  }


  /* MAIN METHODS */
  clear() {
    var c = this.c;
    // border width, around 2px
    c.lineWidth = c.canvas.width/400;
    var width = c.canvas.width;
    var height = c.canvas.height;
    c.clearRect(0, 0, width, height); // Clear
    c.strokeRect(0, 0, width, height); // Bounding box
  }


  draw() {
    this.clear();
    // Draw data
    let f = this.fields;
    let data = this.dataOhlc;
    let cw = this.candleWidth;
    // TODO: Convert the data to json in the constructor
    // and remove this if/else, so that we can support arrays also
    var l = this.fLabels;
    for (var i in data) {
      var c = new Candle(this.c,
        i*this.spacing,   // date
        data[i][l.open],  // open
        data[i][l.high],  // high
        data[i][l.low],   // low
        data[i][l.close], // close
        cw,
      );
      c.maxY = this.max;
      c.minY = this.min;
      c.volH = 0;
      if (this.dataPoints.length != 0) {
        c.volH = this.volH;
      }
      if (this.settings.showVolume) {
        c.volH = this.volH;
      }
      c.draw();
    }
    // Vertical lines
    let lines = this.vLines;
    if (lines.length != 0) {
      var c = this.c;
      for (var i in data) {
        // Line
        if (lines.find((line) => {
          // Only lines for which there is a datapoint
          return data[i].date == line.date;
        }) != null) {
          // Draw line
          c.save();
          c.setLineDash([5, 3]);/*dashes are 5px and spaces are 3px*/
          c.beginPath();
          c.strokeStyle = 'rgba(0, 0, 100, 0.4)'; // black
          c.lineWidth = 2;
          c.moveTo(i*this.spacing+cw/2, 0);
          c.lineTo(i*this.spacing+cw/2, c.canvas.height);
          c.stroke();
          c.restore();
        }
      } //for data
    }
    // Horizontal lines
    lines = this.hLines;
    if (lines.length != 0) {
      var c = this.c;
      for (var i of lines) {
        // Draw line
        c.save();
        c.setLineDash([5, 3]);/*dashes are 5px and spaces are 3px*/
        c.beginPath();
        c.strokeStyle = 'rgba(0, 0, 100, 0.4)'; // black
        c.lineWidth = 2;
        c.moveTo(0, this._translate(i));
        c.lineTo(c.canvas.width, this._translate(i));
        c.stroke();
        c.restore();
      } //for data
    }
    this.drawDataOther();
    this.drawDataPoints();
    if (this.settings.showVolume) {
      this.drawVolume();
    }
  } // draw()


  drawCrosshair(x, y) {
    let c = this.c;
    this.draw();
    c.save();
    c.setLineDash([5, 3]); // dashes are 5px and spaces are 3px
    c.strokeStyle = 'rgba(0, 0, 100, 0.4)'; // black
    c.lineWidth = 2;
    // HORIZ
    c.beginPath();
    c.moveTo(0, y);
    c.lineTo(c.canvas.width, y);
    c.stroke();
    // VERT
    c.beginPath();
    c.moveTo(x, 0);
    c.lineTo(x, c.canvas.height);
    c.stroke();
    c.restore();
  }


  drawDataOther() {
    const c = this.c;
    if (this.dataOther.length == 0) return;
    for (let dataset of this.dataOther) {
      if (dataset.color) {
        c.strokeStyle = dataset.color;
      }
      // TODO: Move to data[0]
      let first = true;
      let data = this.dataOhlc;
      for (var i in data) {
        // Line
        let point = dataset.data.find((point) => {
          return data[i].date == point.date;
        });
        if (point != null) {
          // Only lines for which there is a datapoint
          //if (i == 0) continue;
          if (first) {
            c.beginPath();
            c.moveTo(i*this.spacing, this._translate(dataset.data[0].value));
            first = false;
            continue;
          }
          c.lineTo(i*this.spacing, this._translate(point.value));
        }
      }
      c.stroke();
      c.strokeStyle = 'black';
    }
  }


  drawDataPoints() {
    // TODO: Remove the 'dataset' code. There is only one dataset for points
    const c = this.c;
    if (this.dataPoints.length == 0) return;
    // Border
    c.strokeRect(0, c.canvas.height-this.volH, c.canvas.width, this.volH);
    const cw = this.candleWidth;
    for (let dataset of this.dataPoints) {
      let first = true;
      let data = this.dataOhlc;
      for (var i in data) {
        // Line
        let point = dataset.data.find((point) => {
          return data[i].date == point.date;
        });
        if (point != null) {
          for (let j = 1; j <= Math.abs(point.value); j++) {
            c.beginPath();
            c.fillStyle = (point.value >= 0)? 'rgb(0,200,0)' : 'rgb(200,0,0)';
            c.fillRect(
              i*this.spacing , // x
              this.canvas.height - j*(this.volH/5) + cw/4, // y
              cw-1, // w
              cw/2, // h
            );
            c.fill();
          }
          if (first) {
            c.beginPath();
            c.moveTo(i*this.spacing, this._translate(dataset.data[0].value));
            first = false;
            continue;
          }
          c.lineTo(i*this.spacing, this._translate(point.value));
        }
      }
    }
  }


  drawVolume() {
    const volumes = this.dataOhlc.map(i => i.volume);
    const maxVol = Math.max(...volumes);
    const c = this.c;
    if (this.dataOhlc.length == 0) return;
    // Border
    c.strokeRect(0, c.canvas.height-this.volH, c.canvas.width, this.volH);
    const cw = this.candleWidth;
    let first = true;
    for (let i in this.dataOhlc) {
      // For each datapoint
      let p = this.dataOhlc[i];
      //let h = 20;
      let h = (p.volume/maxVol)*this.volH;
      c.beginPath();
      c.fillStyle = (p.close >= p.open)? 'rgb(0,200,0)' : 'rgb(200,0,0)';
      c.fillRect(
        i*this.spacing , // x
        this.canvas.height-h-1, //y, 1 is hardcoded TODO
        cw-1, // w
        h, // h
      );
    }
  }


  // Mouse handlers
  onMouseDown(e) {
    this._mouseDragging = true;
    this._mousePosStart = { x: e.clientX, y: e.clientY };
  }


  onMouseUp(e) {
    this._mouseDragging = false;
  }


  onMouseOut(e) {
    this._mouseDragging = false;
    this.draw();
  }


  onMouseMove(e) {
    if (!e.defaultPrevented) {
      this._onMouseMoveDefault(e);
    }
    if (typeof this.onMouseMoveUser === 'function') {
      // Run user function
      this.onMouseMoveUser(e);
    }
  }


  mouseToIndex(x) {
    // x is e.clientX from a mouse event
    let mousePos = this._mouseToPos(x, 0);
    let i = (mousePos.x - this.candleWidth/2)/this.spacing;
    return Math.round(i);
  }


  drawLine(a, b, label) {
    a = this._mouseToPos(a.x, a.y);
    b = this._mouseToPos(b.x, b.y);
    let c = this.c;
    c.beginPath();
    c.moveTo(a.x, a.y);
    c.lineTo(b.x, b.y);
    c.stroke();
    // Draw text
    let midPoint = {
      x: (a.x+b.x)/2,
      y: (a.y+b.y)/2,
    };
    let h = 30;
    c.fillStyle = 'black';
    c.font = `${h}px monospace`;
    c.save();
    c.translate(midPoint.x, midPoint.y);
    let textMeasure = c.measureText(label);
    const angle = Math.atan2(b.y-a.y, b.x-a.x);
    c.rotate(angle);
    c.fillText(label, -textMeasure.width/2, -h/2);
    c.restore();
  }


  drawTooltip(data) {
    let c = this.c;
    let h = 30;
    c.fillStyle = 'grey';
    c.font = `${h}px monospace`;
    c.fillText("t: " + data.date, 10, 1*h);
    c.fillText("O: " + data.open.toFixed(2), 10, 2*h);
    c.fillText("H: " + data.high.toFixed(2), 10, 3*h);
    c.fillText("L: " + data.low.toFixed(2), 10, 4*h);
    c.fillText("C: " + data.close.toFixed(2), 10, 5*h);
    if (this.settings.showVolume) {
      c.fillText("v: " + data.volume.toFixed(0), 10, 6*h);
    }
  }


  /* PRIVATE */
  _onMouseMoveDefault(e) {
    // Default is to draw crossHair
    this.draw(); // TODO, is this needed?
    if (this.settings.showCrosshair == true) {
      let canvas = this.canvas;
      var rect = canvas.getBoundingClientRect();
      let mousePos = this._mouseToPos(e.clientX, e.clientY);
      this.drawCrosshair(mousePos.x, mousePos.y);
    }
    if (this.settings.hoverLabels) {
      let i = this.mouseToIndex(e.clientX);
      if (i < this.dataOhlc.length) {
        //this.draw();
        this.drawTooltip(this.dataOhlc[i]);
      }
    }
    if (this._mouseDragging) {
      // TODO: Move into defaultPrevented
      this._mousePosEnd = { x: e.clientX, y: e.clientY };
      let iStart = this.mouseToIndex(this._mousePosStart.x);
      let iEnd = this.mouseToIndex(this._mousePosEnd.x);
      if (iEnd < this.dataOhlc.length) {
        let pStart = this.dataOhlc[iStart].close;
        let pEnd = this.dataOhlc[iEnd].close;
        let perc = 100*(pEnd - pStart)/pStart;
        this.drawLine(this._mousePosStart, this._mousePosEnd, perc.toFixed(2) + '%');
      }
    }
  }


  _translate(y) {
    let H = this.canvas.height;
    if (this.dataPoints.length != 0) {
      H = this.canvas.height - this.volH;
    }
    let MIN = this.min;
    let MAX = this.max;
    const ret = H - H*(y-MIN)/(MAX-MIN);
    return ret;
  }


  _mouseToPos(x, y) {
    // x: mouseEvent.clientX
    // y: mouseEvent.clientY
    let c= this.canvas;
    let rect = c.getBoundingClientRect();
    return {
      x: (x - rect.left)*(c.width/c.clientWidth),
      y: (y - rect.top)*(c.height/c.clientHeight),
    };
  }

}; // class Plot


class Candle {
  // Draw a OHLC box/candle
  volH = 0;
  constructor(context, x, open, high, low, close, width = null) {
    // Check
    if (context == null) throw 'Context undefined';
    if (x == null) throw 'X undefined';
    if (open == null) throw 'open undefined';
    // Params
    this.c = context;
    this.x = x; // X position
    this.open = open;
    this.high = high;
    this.low = low;
    this.close = close;
    // Other
    this.cHeight = context.canvas.height;
    this.candleWidth = (width != null)? width : 10;
    this.maxY = 100;
    this.minY = 0;
  }


  draw() {
    let c     = this.c;
    let cw    = this.candleWidth;
    let open  = this.open;
    let high  = this.high;
    let low   = this.low;
    let close = this.close;
    // Candlestick: red/green
    if (close >= open) {
      c.fillStyle = 'rgb(0, 200, 0)';
      c.strokeStyle = 'rgb(0, 200, 0)';;
    } else {
      c.fillStyle = 'rgb(200, 0, 0)';
      c.strokeStyle = 'rgb(200, 0, 0)';
    }
    // Wick: high to low
    c.beginPath();
    c.lineWidth = 2;
    c.moveTo(this.x + cw/2, this.translate(high));
    c.lineTo(this.x + cw/2, this.translate(low));
    c.stroke();
    // Candlestick body: top to bottom
    c.beginPath();
    c.fillRect(
      this.x,
      this.translate(Math.max(open, close)),
      this.candleWidth,
      Math.abs(this.translate(close) - this.translate(open)),
    );
    c.strokeStyle = 'black'; // reset
  }


  translate(y) {
    var H = this.cHeight - this.volH;
    var MIN = this.minY;
    var MAX = this.maxY;
    const ret = H - H*(y-MIN)/(MAX-MIN);
    return ret;
  }


}; //class Candle
