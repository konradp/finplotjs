var express = require('express');
var app = express();
var path = require('path');
var port = 8001;

// Serve from 'public' dir
app.use(express.static('public'))

console.log('Listening on port', port);
app.listen(port);
